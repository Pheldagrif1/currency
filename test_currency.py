import pytest
from convert import convert_amount


@pytest.mark.parametrize("num1, rate, expected", [(100, .1, 10), (100, .25, 25),(0, .5, 0)])
def test_convert_amount(num1, rate, expected):
    assert convert_amount(num1, rate) == expected
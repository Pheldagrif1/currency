import requests
from convert import convert_amount


def get_from_currancy():
    print("You can convert from: \nUSD - United States Dollars\nEUR - Euro\nJPY - Japanese Yen\nMXN - Mexican Peso")
    print("If you know any other currency codes you can enter those too, but I'm not validating them.")
    print("So you'd better know what you are doing!")
    currency_to_convert = input("Enter the 3 digit code for the currency you want to convert: ")
    is_ok = validate_input(currency_to_convert)
    if is_ok == False:
        while is_ok == False:
            currency_to_convert = input("Enter the 3 digit code for the currency you want to convert: ")
            is_ok = validate_input(currency_to_convert)

    return currency_to_convert

def get_to_currancy():
    currency_to_convert_to = input("Enter the 3 digit code for the currency you want to convert into: ")
    is_ok = validate_input(currency_to_convert_to)
    if is_ok == False:
        while is_ok == False:
            currency_to_convert_to = input("Enter the 3 digit code for the currency you want to convert into: ")
            is_ok = validate_input(currency_to_convert_to)

    return currency_to_convert_to


def validate_input(currency_to_convert):
    valid = True

    if currency_to_convert.isalpha() == False or len(currency_to_convert) != 3: 
        print("Please enter a three character, alpha-only string")
        valid = False
    return valid


def get_conversion_amt():
    is_ok = False
    while is_ok == False:
        conversion_amt = input("Please enter the amount to convert: ") 
        if validate_number(conversion_amt) == True:
            is_ok = True
        else:
            print("Please enter only numeric digits and a decimal")
    return conversion_amt

def validate_number(number):
    try:
        float(number)
        return True
    except ValueError:
        return False

def call_api(url):
    try:
        response = requests.get(url)
        data = response.json()
        return data
    except requests.exceptions.HTTPError as errh:
        print("An Http Error occurred:" + repr(errh))
        return "error"
    except requests.exceptions.ConnectionError as errc:
        print("An Error Connecting to the API occurred:" + repr(errc))
        return "error"
    except requests.exceptions.Timeout as errt:
        print("A Timeout Error occurred:" + repr(errt))
        return "error"
    except requests.exceptions.RequestException as err:
        print("An Unknown Error occurred" + repr(err))
        return "error"
    

    

################################################# main logic ###########################################
currency = get_from_currancy()
currency = currency.upper()


# Where USD is the base currency you want to use
#url = 'https://v6.exchangerate-api.com/v6/92005635ee30a871396eb1d1/latest/USD'
url = 'https://v6.exchangerate-api.com/v6/XXXXXXXXXXXXXXXXXXXXXXXXXXX/latest/' + currency
data = call_api(url)

if data == "error":
    print("Sorry, something went wrong with the API call!")
else:
    if data["result"] == "error":
        print("Sorry, that wasn't a valid code!  I warnd you!  Bye!")
    else:
        to_currency = get_to_currancy()
        to_currency = to_currency.upper()
        conversion_amount = get_conversion_amt()
        conversion_table = data["conversion_rates"]
        try:
            conversion_rate = conversion_table[to_currency]
        except KeyError:
            print("Sorry, that wasn't a valid code to convert to!  I am just going to give you USD.")
            conversion_rate = conversion_table["USD"]
            to_currency = "USD"

        converted_amt = convert_amount(conversion_amount, conversion_rate)
        print (f"${float(conversion_amount):.2f} {currency} is worth ${converted_amt:.2f} in {to_currency}.")

        

def convert_amount(conversion_amount, conversion_rate):
    
    new_amount = float(conversion_amount) * conversion_rate
    return new_amount